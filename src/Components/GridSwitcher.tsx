import React from 'react';
import { Icon } from './Icon';
import "./GridSwitcher.scss";
import { scrollToTop, viewSwitchKey } from './helpers/constants';
import classNames from 'classnames';


export enum ViewTypes {
    grid = 'grid',
    list = 'list',
}

interface GridSwitcher {
    selectedView: ViewTypes;
    changeView: (view: ViewTypes) => void;
}

const GridSwitcher = ({ selectedView, changeView }: GridSwitcher) => {
    const viewButtons = Object.values(ViewTypes);

    const handleViewChange = (view: ViewTypes) => {
        localStorage.setItem(viewSwitchKey, view);
        changeView(view);

        scrollToTop();
    }

    return (
        <div className="grid-switcher">
            <div className="grid-switcher-inner">
                {viewButtons.map(button =>
                    <button key={button}
                        onClick={() => handleViewChange(button)}
                        className={classNames({
                            'grid-switcher-item': true,
                            'selected': button === selectedView,
                        })}>
                        <Icon icon={button as any} size={24} />
                    </button>
                )}
            </div>
        </div>
    )
}

export default GridSwitcher;
