import React from "react";


export enum Icons {
    shiba = 'shiba',
    grid = 'grid',
    list = 'list',
}

interface IconProps {
    icon: Icons;
    size?: number;
}

export const Icon = ({
    icon,
    size = 12,
}: IconProps) => (
    <svg
        className="svg-icon"
        width={size}
        height={size}
    >
        <use href={`#${icon}`} />
    </svg>
);
