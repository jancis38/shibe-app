import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Icons } from '../../Icon';
import "./Logo.scss";


const Logo = ({ removeSelected }: { removeSelected: () => void }) => {
    const searchParams = new URLSearchParams(window.location.search);
    const artist = searchParams.get('artist');
    const title = searchParams.get('title');

    return (
        <div className="logo">
            <Link to="/"
                className="logo-icon"
                onClick={removeSelected}
            >
                <Icon icon={Icons.shiba} size={48} />
            </Link>
            <div className="logo-title">
                Shibe
                {artist && title &&
                    <>
                        :&nbsp;
                        <span className="bold">
                            {artist} - {title}
                        </span>
                    </>
                }
            </div>
        </div>
    )
}

export default Logo;
