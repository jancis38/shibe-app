import React from 'react';
import "./Header.scss";
import Logo from './Logo/Logo';
import Quote from './Quote/Quote';


const Header = ({ removeSelected }: { removeSelected: () => void }) => {
    return (
        <header>
            <Logo removeSelected={() => removeSelected()} />
            <Quote />
        </header>
    )
}

export default Header;
