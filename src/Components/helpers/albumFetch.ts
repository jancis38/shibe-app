import axios from "axios";
import { useEffect, useState } from "react";


export interface Album {
    ranking: number;
    title: string;
    artist: string;
}

interface CoverImage {
    [number: number]: string;
}

export const albumFetch = () => {
    const [coverImages, setCoverImages] = useState<CoverImage>({});
    const [coversLoaded, setCoversLoaded] = useState(false);

    const [albums, setAlbums] = useState<Album[]>([]);
    const [albumsLoaded, setAlbumsLoaded] = useState(false);

    const getCovers = async () => {
        let response = await axios({
            method: 'GET',
            url: 'https://jurg.is/images.json',
        });

        if (response.status === 200 && response.data) {
            setCoverImages(response.data);
        }

        setCoversLoaded(true);
    }

    const getAlbumInfo = async () => {
        if (!!Object.keys(coverImages).length) {
            let response = await axios({
                method: 'GET',
                url: 'https://jurg.is/data.txt',
            });

            const albumItems: Album[] = [];
            const albums: string[] = response.data.split(/\r?\n/);

            albums.map((album, index) => {
                if (!index) return;
                const splitInfo = album.split(',');

                albumItems.push({
                    ranking: Number(splitInfo[0]),
                    title: splitInfo[1].trim(),
                    artist: splitInfo[2].trim(),
                });
            });

            setAlbumsLoaded(true);
            setAlbums(albumItems.sort(
                (a, b) => a.ranking - b.ranking,
            ));
        }
    }

    useEffect(() => {
        getCovers();
    }, []);

    useEffect(() => {
        if (coversLoaded) {
            getAlbumInfo();
        }
    }, [coversLoaded]);

    return {
        albums,
        coverImages,
        loaded: albumsLoaded && coversLoaded,
    }
}
