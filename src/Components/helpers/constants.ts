export const viewSwitchKey = 'listView';
export const imageFallback = 'https://www.allianceplast.com/wp-content/uploads/no-image.png';

export const scrollToTop = () => window.scrollTo(0, 0);
