import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';

import Header from './Components/Header/Header';
import Albums from './Views/Albums/Albums';


const App = () => {
    const [selectedAlbum, setAlbum] = useState<number | null>(null);

    const changeAlbum = (id: number) => {
        setAlbum(id);
    }

    const removeSelected = () => {
        setAlbum(null);
    }

    return (
        <>
            <Header removeSelected={removeSelected} />
            <Albums
                selectedAlbum={selectedAlbum}
                changeAlbum={changeAlbum}
            />
        </>
    )
}

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
document.getElementById('app'));
