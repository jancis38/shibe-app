import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import "./Albums.scss";
import GridSwitcher, { ViewTypes } from '../../Components/GridSwitcher';
import { imageFallback, viewSwitchKey } from '../../Components/helpers/constants';
import { albumFetch } from '../../Components/helpers/albumFetch';
import AlbumView from './../AlbumView/AlbumView';


interface AlbumsProps {
    selectedAlbum: number | null;
    changeAlbum: (id: number) => void;
}

const Albums = ({ selectedAlbum, changeAlbum }: AlbumsProps) => {
    const isAlbumSelected = selectedAlbum !== null;
    const defaultView = localStorage.getItem(viewSwitchKey) as ViewTypes || ViewTypes.grid;
    const [selectedView, setView] = useState(defaultView);

    const {
        albums,
        coverImages,
        loaded,
    } = albumFetch();

    return (
        <>
            <main>
                {loaded && !!albums.length && !isAlbumSelected &&
                    <div className={classNames({
                        'albums': true,
                        'grid': selectedView === ViewTypes.grid,
                        'list': selectedView === ViewTypes.list,
                    })}>
                        {albums.map((album, index: number) =>
                            <Link key={index}
                                to={`?artist=${album.artist}&title=${album.title}`}
                                onClick={() => changeAlbum(index)}
                                className="albums-cover"
                            >
                                {selectedView === ViewTypes.list &&
                                    <div className="albums-cover-rank">
                                        {album.ranking}
                                    </div>
                                }
                                <img
                                    src={coverImages[index] || imageFallback}
                                    loading="lazy"
                                    alt={album.title}
                                />
                                <div className="albums-info">
                                    <span className="albums-info-title">
                                        {selectedView === ViewTypes.grid &&
                                            <div className="albums-cover-rank">
                                                #{album.ranking}
                                            </div>
                                        }
                                        {album.title}
                                    </span>
                                    <span className="albums-info-artist">
                                        by {album.artist}
                                    </span>
                                </div>
                            </Link>
                        )}
                    </div>
                }
                {loaded && isAlbumSelected &&
                    <AlbumView
                        ranking={albums[selectedAlbum].ranking}
                        artist={albums[selectedAlbum].artist}
                        title={albums[selectedAlbum].title}
                        coverImage={coverImages[selectedAlbum]}
                    />
                }

                {loaded && !albums.length &&
                    <div className="message">No albums loaded.</div>
                }
                {!loaded && <div className="message">Loading...</div>}
            </main>

            {!isAlbumSelected &&
                <GridSwitcher
                    selectedView={selectedView}
                    changeView={(view) => setView(view)}
                />
            }
        </>
    )
}

export default Albums;
