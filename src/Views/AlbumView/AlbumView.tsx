import React from 'react';
import { Album } from '../../Components/helpers/albumFetch';
import { scrollToTop } from '../../Components/helpers/constants';
import "./AlbumView.scss";


interface AlbumViewProps extends Album {
    coverImage: string;
}

const AlbumView = ({
    // ranking,
    // title,
    // artist,
    coverImage,
}: AlbumViewProps) => {
    scrollToTop();

    return (
        <div className="album-container">
            <div className="album-view" style={{
                backgroundImage: `url('${coverImage}')`,
            }} />
        </div>
    )
}

export default AlbumView;
